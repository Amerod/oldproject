<!DOCTYPE html>
<html lang="en">
<head>
	<title>La Casa - Profile</title>
	<meta charset="utf-8">
	
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.vertical-tabs.css">

</head>
<body>

	<section class="hero">
		<?php include ROOT . '/view/header.php'; ?>
			<div>
				<section class="www userprof container" style="padding-top: 15px;">
					<div class="tabs">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#profile" data-toggle="tab">Профиль</a></li>
							<li><a href="#applications" data-toggle="tab">Заявки</a></li>
							<li><a href="#ViewApplications" data-toggle="tab">Просмотр заявок</a></li>
							<li><a href="#AddHotel" data-toggle="tab">Добавить отель</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="profile">
								<form method="post" action="">
									<?php if (isset($errorUser) && is_array($errorUser)): ?>
										<ul style="list-style-type: none;">
											<?php foreach ($errorUser as $errorU): ?>
												<li><?php echo $errorU; ?></li>
											<?php endforeach; ?>
										</ul>
									<?php endif; ?>
									
									<h4>Фамилия:</h4>
									<input type="text" name="LName" placeholder="Фамилия" value="<?php echo $UserInfo->LName; ?>">
									<h4>Имя:</h4>
									<input type="text" name="FName" placeholder="Имя" value="<?php echo $UserInfo->FName; ?>">
									<h4>Отчество:</h4>
									<input type="text" name="MName" placeholder="Отчество" value="<?php echo $UserInfo->MName; ?>">
									<br>
									<h4>День рождения:</h4>
									<input type="text" name="birthday" placeholder="День рождения" value="<?php echo $UserInfo->birthDay; ?>">
									<br>
									<h4>Номер телефона:</h4>
									<input type="text" name="phone" placeholder="Номер телефона" value="<?php echo $UserInfo->phone; ?>">
									<p>Оператор позвонит вам для уточнения информации и подтверждении заказа.</p>
									<input type="submit" name="submitProf" value="Сохранить">
								</form>
								<hr color="#fff">
								<form method="post" action="">
									<h4>Смена пароля:</h4>
									<input class="wow" type="text" name="oldPass" placeholder="Ваш текущий пароль" style="margin-bottom: 10px;">
									<br>
									<input class="wow" type="text" name="newPass" placeholder="Новый пароль" style="margin-bottom: 10px;"> <?php if ($errorPass == false): ?>
												<?php echo 'Пароль сохранён!'; ?>
											<?php elseif(is_array($errorPass)): ?>
												<?php foreach ($errorPass as $errorP): ?>
													<?php echo $errorP; ?>
												<?php endforeach; ?>
											<?php endif; ?>
									<br>
									<input type="submit" name="submitPass" value="Сменить пароль">
								</form>
							</div>
							<div class="tab-pane" id="applications">
								<!-- Тут будут заявки (именно пользователя) -->
									<table width="100%" class="colorF">
										<tr>
											<th>Название Отеля</th>
											<th>Создание заявки</th>
											<th>Цена</th>
											<th>Статус</th>
										</tr>
										<div class="infoDiv">
											<tr align="center">
												<!-- Вывод в виде цыкла -->
												<td><?php echo $OrdUser->title; ?></td>
												<td><?php echo $OrdUser->Dat; ?></td>
												<td><?php echo $OrdUser->price; ?></td>
												<td><?php echo $OrdUser->status; ?></td>
											</tr>
										</div>
									</table>
							</div>
							<div class="tab-pane" id="ViewApplications">
								<!-- Берёться с таблицы заявок (именно все для определённого отеля) -->
								<table width="100%" class="colorF">
									<tr align="center">
										<td>ФИО</td>
										<td>Дата заявки</td>
										<td>Дата заезда</td>
										<td>Цена</td>
										<form action="" method="post">
											<td>Статус<br>
												<!-- <select>
													<option selected="selected" value="1">Заявка рассматривается</option>
													<option value="2">Подтверждено</option>
													<option value="3">Отказано</option>
												</select> -->
											</td>
										</form>
									</tr>
								</table>
							</div>
							<div class="tab-pane" id="AddHotel">
								<form method="post" action="">
									<h4>Название отеля:</h4>
									<input type="text" placeholder="Название отеля">
									<h4>Полное описание отеля:</h4>
									<input type="text" placeholder="Описание">
									<h4>Путь картинки:</h4>
									<input type="text" placeholder="URL">
									<h4>Цена:</h4>
									<input type="text" placeholder="Цена">
									<h4>Дата создания отеля:</h4>
									<input type="text" placeholder="00.00.000">
									<h4>Кухня:</h4>
									<input type="text" placeholder="Если имеются">
									<h4>Кровать:</h4>
									<input type="text" placeholder="Если имеются">
									<h4>Ванная:</h4>
									<input type="text" placeholder="Если имеются" style="margin-bottom: 10px;">
									<br>
									<input type="submit" name="submitAdd" value="Добавить">
								</form>
							</div>
						</div>
					</div>
				</section>
			</div>
	</section><!--  end hero section  -->


	<footer>
		<div class="copyrights wrapper">
			2018 VLD
		</div>
	</footer><!--  end footer  -->

	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	
</body>
</html>