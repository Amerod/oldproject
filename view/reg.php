<!DOCTYPE html>
<html lang="en">
<head>
	<title>La Casa - Registr</title>
	<meta charset="utf-8">
		
	<link rel="stylesheet" type="text/css" href="/css/reset.css">
	<link rel="stylesheet" type="text/css" href="/css/responsive.css">

</head>
<body>

	<section class="hero">
		<?php include ROOT . '/view/header.php'; ?>

			<section class="reg left1-block www" align="center">
				<!-- тут будет блок авторизации -->
					<div>
						<br>
						<h3>Регистрация</h3>
					</div>
					<?php if ($result): ?>
						<p>Вы зарегестрированы!</p>
					<?php else: ?>
				<form method="post" action="">
   					<div>
   						<p style="margin-top: 15px;">Имя</p><br>
            			<input type="text" name="FName" value="<?php echo $FName; ?>">
       					<p style="margin-top: 15px;">Фамилия</p><br>
            			<input type="text" name="LName" value="<?php echo $LName; ?>">
            			<p style="margin-top: 15px;">Отчество</p><br>
            			<input type="text" name="MName" value="<?php echo $MName; ?>">
            			<p style="margin-top: 15px;">День рождения</p><br>
            			<input type="text" name="BDay" placeholder="00.00.0000">
            			<p style="margin-top: 15px;">Номер телефона</p><br>
            			<input type="text" name="phone" value="<?php echo $phone; ?>">
            			<p style="margin-top: 15px;">Пароль</p><br>
            			<input type="text" name="password">
       				</div>
       					<p style="margin-top: 15px;"><input type="submit" name="registr" value="Войти"></p>
				</form>
					<?php endif; ?>
			</section>
				<section class="right1-block www1">
					<h3 style="color: #fff;">Исправте ошибки</h3>
					<?php if (isset($errors) && is_array($errors)): ?>
						<ul>
							<?php foreach ($errors as $error): ?>
								<li><?php echo '- ' . $error; ?></li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</section>
	</section><!--  end hero section  -->


	<footer>
		<div class="copyrights wrapper">
			2018 VLD
		</div>
	</footer><!--  end footer  -->
	
</body>
</html>