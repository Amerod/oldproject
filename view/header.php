<?php require_once ROOT . '/models/Login.php'; ?>
		<header>
			<div class="wrapper">
				<a href="/news"><img src="../img/logo.png" class="logo" alt="" titl=""/></a>
				<nav>
					<ul>
						<li><a href="/news">Преобрести Отель</a></li>
						<li><a href="#">О нас</a></li>
						<li><a href="#">Контакты</a></li>
					</ul>
					<?php if (Login::isGuest()): ?>
						<a href="/login" class="login_btn">Вход</a>
					<?php else: ?>
						<a href="/logout" class="login_btn">Выход</a>
						<a href="/user" class="login_btn">Профиль</a>
					<?php endif; ?>
				</nav>
			</div>
		</header><!--  end header section  -->