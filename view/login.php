<!DOCTYPE html>
<html lang="en">
<head>
	<title>La Casa - Login</title>
	<meta charset="utf-8">
	
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

</head>
<body>

	<section class="hero">
		<?php include ROOT . '/view/header.php'; ?>

			<section class="www" align="center">
				<!-- тут будет блок авторизации -->
					<div>
						<br>
						<h3>Авторизация</h3>
					</div>
					<?php if (isset($errors) && is_array($errors)): ?>
						<ul style="list-style-type: none;">
							<?php foreach ($errors as $error): ?>
								<li>- <?php echo $error; ?></li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				<form method="post" action="">
   					<div>
   						<p style="margin-top: 15px;">Номер телефона</p><br>
            			<input type="text" name="login">
   					</div>
       				<div>
       					<p style="margin-top: 15px;">Пароль</p><br>
            			<input type="text" name="password">
       				</div>
       					<p style="margin-top: 15px;"><input type="submit" name="submit" value="Войти"></p>
				</form>
					<div>
						<hr width="100%" align="center" color="#fff">
						<a href="#">Забыли пароль?</a><br>
						<a href="/reg">Регистрация</a>
					</div>
				<!-- регистрация и забыл пароль -->
			</section>
	</section><!--  end hero section  -->


	<footer>
		<div class="copyrights wrapper">
			2018 VLD
		</div>
	</footer><!--  end footer  -->
	
</body>
</html>