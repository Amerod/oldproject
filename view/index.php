<!DOCTYPE html>
<html lang="en">
<head>
	<title>La Casa</title>
	<meta charset="utf-8">
	
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</head>
<body>

	<section class="hero">
		<?php include ROOT . '/view/header.php'; ?>
			<section class="caption">
				<h2 class="caption">Find Your Dream Hotel</h2>
				<h3 class="properties">- Hotels -</h3>
			</section>
	</section><!--  end hero section  -->


	<section class="search">
		<div class="wrapper">
			<form action="#" method="post">
				<input type="text" id="search" name="search" placeholder="Что вы желаете найти?"  autocomplete="off"/>
				<input type="submit" id="submit_search" name="submit_search"/>
			</form>
			<a href="#" class="advanced_search_icon" id="advanced_search_btn"></a>
		</div>

		<div class="advanced_search">
			<div class="wrapper">
				<span class="arrow"></span>
				<form action="#" method="post">
					<div class="search_fields">
					</div>
					<div class="search_fields">
						<input type="text" class="float" id="min_price" name="min_price" placeholder="Минимальная Цена"  autocomplete="off">

						<hr class="field_sep float"/>

						<input type="text" class="float" id="max_price" name="max_price" placeholder="Максимальная Цена"  autocomplete="off">
					</div>
						<!-- Доделать кнопку --><input type="submit" class="more_listing more_listing_btn" name="submit_search"/>
				</form>
			</div>
		</div><!--  end advanced search section  -->
	</section><!--  end search section  -->


	<section class="listings">
		<div class="wrapper">
			<ul class="properties_list">
				<?php foreach ($newsList as $newsItem):?>
				<li>
					<a href="/news/<?php echo $newsItem['id']; ?>">
						<img src="<?php echo $newsItem['UrlImg']; ?>" alt="" title="" class="property_img"/>
					</a>
					<span class="price">$<?php echo $newsItem['price']; ?></span>
					<div class="property_details">
						<h1>
							<a href="/news/<?php echo $newsItem['id']; ?>"><?php echo $newsItem['title']; ?></a>
						</h1>
						<h2><?php echo $newsItem['kitchens'] . ' кухни ' . $newsItem['bed'] . ' кровати ' . $newsItem['bath'] . ' ванные '?></h2>
					</div>
				</li>
				<?php endforeach ?>
			</ul>
			<div class="more_listing">
				<!-- сделать из ссылки кнопку --><a href="#" class="more_listing_btn">показать больше?</a>
			</div>
		</div>
	</section>	<!--  end listing section  -->

	<footer>
		<div class="copyrights wrapper">
			2018 VLD
		</div>
	</footer>
	
</body>
</html>