<!DOCTYPE html>
<html lang="en">
<head>
	<title>La Casa - <?php echo $newsItem->title; ?></title>
	<meta charset="utf-8">
	
	<link rel="stylesheet" type="text/css" href="../css/reset.css">
	<link rel="stylesheet" type="text/css" href="../css/responsive.css">

</head>
<body>

	<section class="hero">
		<?php include ROOT . '/view/header.php'; ?>
		<section class="www userprof">
			<div class="right-block">
				<img class="img-max" src="<?php echo $newsItem->UrlImg; ?>" alt="Фотография отеля">
				<br>
				<h3><?php echo $newsItem->title; ?></h3>
				<p><?php echo $newsItem->fulltitle; ?></p>
			</div>
			<div class="left-block" align="center">
				<h3>Преобрести номер</h3><hr>
				<form method="post">
					<button class="bay" name="addOrd"><h3><?php echo $newsItem->price; ?>$</h3></button>
				</form>
				<br>
				<h3>Кухни</h3>
				<p><?php echo $newsItem->kitchens; ?></p>
				<h3>Кровати</h3>
				<p><?php echo $newsItem->bed; ?></p>
				<h3>Ванные</h3>
				<p><?php echo $newsItem->bath; ?></p>
			</div>
		</section>
	</section><!--  end hero section  -->

	<footer>
		<div class="copyrights wrapper">
			2018 VLD
		</div>
	</footer><!--  end footer  -->
	
</body>
</html>