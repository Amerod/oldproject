<?php

require_once ROOT . '/models/Login.php';

class LoginController
{
	
	public function actionsLogin()
	{
		$login = '';

		$errors = false;

		if (isset($_POST['submit'])) {
			$login = $_POST['login'];
			$password = $_POST['password'];

		// Проверка полей
		if (!Login::checkLogin($login)) {
			$errors[] = 'Неправильный номер телефона';
		}

		if (!Login::checkPassword($password)) {
			$errors[] = 'Пароль не должен быть короче 6 символов';
		}

		// Проверка на существование пользователя
		$userId = Login::checkUserData($login, $password);

		if ($userId == false) {
			$errors[] = 'Неправильные данные для входа';
		}
		else {
			// Если всё верно запомнить пользователя
			Login::auth($userId);

			header("Location: /user");
		}
	}

		require_once (ROOT . '/view/login.php');

		return true;
	}

	public function actionsLogout() {
		unset($_SESSION['user']);
		header("Location: /news");
	}
}