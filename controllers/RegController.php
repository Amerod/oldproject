<?php
// Подключаю правила
require_once ROOT . '/models/Reg.php';

class RegController
{
	
	public function actionsReg()
	{
		$FName = '';
		$LName = '';
		$MName = '';
		$BDay = '';
		$phone = '';

		$errors = false;

			if (isset($_POST['registr'])) {
				$FName = $_POST['FName'];
				$LName = $_POST['LName'];
				$MName = $_POST['MName'];
				$BDay = $_POST['BDay'];
				$phone = $_POST['phone'];
				$password = $_POST['password'];

				if (!Rule::checkFName($FName)) {
						$errors[] = 'Имя должно быть больше 2-х букв'; //Выводящий текс на экран о совершённой ошибке
				}

				if (!Rule::checkLName($LName)) {
					$errors[] = 'Фамилия должно быть больше 2-х букв'; //Выводящий текс на экран о совершённой ошибке
				}

				if (!Rule::checkMName($MName)) {
					$errors[] = 'Отчество должно быть больше 2-х букв'; //Выводящий текс на экран о совершённой ошибке
				}

				if (!Rule::checkBDay($BDay)) {
					$errors[] = 'Дата должна соответствовать маске'; //Выводящий текс на экран о совершённой ошибке
				}

				if (!Rule::checkPhone($phone)) {
					$errors[] = 'Номер телефона должен быть больше 8 символов'; //Выводящий текс на экран о совершённой ошибке
				}

				if (Rule::checkPhoneExists($phone)) {
					$errors[] = 'Номер телефона зарегестрирован';
				}

				if (!Rule::checkPassword($password)) {
					$errors[] = 'Пароль должен быть больше 6 символов'; //Выводящий текс на экран о совершённой ошибке
				}
				
				if ($errors == false) {
					// зарегестрировать пользователя
					$result = Rule::Register($FName, $LName, $MName, $BDay, $phone, $password);
				}
			}

		require_once (ROOT . '/view/reg.php');

		return true;
	}
}