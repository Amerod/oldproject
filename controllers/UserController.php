<?php

require_once ROOT . '/models/Login.php';
require_once ROOT . '/models/User.php';

class UserController
{
	
	public function actionsIndex()
	{
		$userId = Login::checkLogged();
		$UserInfo = User::checkUserData($userId);

		$errorPass = false;
		$errorUser = false;

		if (isset($_POST['submitProf'])) {
				$LName = $_POST['LName'];
				$FName = $_POST['FName'];
				$MName = $_POST['MName'];
				$BDay = $_POST['birthday'];
				$phone = $_POST['phone'];
				
				//проверка полей на валидность
				if (!User::checkLName($LName)) {
					$errorUser[] = 'Фамилия должна быть больше 2-х букв'; //Выводящий текс на экран о совершённой ошибке
				}

				if (!User::checkFName($FName)) {
					$errorUser[] = 'Имя должна быть больше 2-х букв'; //Выводящий текс на экран о совершённой ошибке
				}

				if (!User::checkMName($MName)) {
					$errorUser[] = 'Отчество должна быть больше 2-х букв'; //Выводящий текс на экран о совершённой ошибке
				}

				if (!User::checkBDay($BDay)) {
					$errorUser[] = 'Дата должна соответствовать маске'; //Выводящий текс на экран о совершённой ошибке
				}

				if (!User::checkPhone($phone)) {
					$errorUser[] = 'Номер телефона должен быть больше 8 символов'; //Выводящий текс на экран о совершённой ошибке
				}
				
				// если нету ошибок изменит данные
				if ($errorUser == false) {
					User::updateProf($userId, $LName, $FName, $MName, $BDay, $phone);

					//timeout и переотравить в /user
					header('Location: /user');
				}
		}

		if (isset($_POST['submitPass'])) {
				$newPass = $_POST['newPass'];
				$oldPass = $_POST['oldPass'];

				if (!User::newPasswordUser($newPass)) {
					$errorPass[] = 'Пароль не должен быть короче 6 символов';
				}

				if ($errorPass == false) {
					User::updatePassword($userId, $oldPass, $newPass);
				}
			}

		// Заявки пользователя
		if ($userId == true)
		{
			User::OrdId($userId);
		}
		

		// Просмотр всех заявок (только модератор)

		// Отправка данных при просмотре заявок 
		// if (condition) {
		// 		# code...
		// 	}

		// Добовление отеля
		// if (condition) {
		// 	# code...
		// }

		require_once (ROOT . '/view/prof.php');

		return true;
	}

}