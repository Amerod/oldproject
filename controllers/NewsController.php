<?php

require_once ROOT . '/models/News.php';
require_once ROOT . '/models/Login.php';

class NewsController
{

	public function actionsIndex()
	{
		$newsList = array();
		$newsList = News::getNewsList();

		require_once (ROOT . '/view/index.php');

		return true;
	}

	public function actionsView($idHotel)
	{
		if ($idHotel) {
			$userId = Login::checkLogged();
			$newsItem = News::getNewsItemById($idHotel);

			require_once (ROOT . '/view/indexId.php');

			if(isset($_POST['addOrd']))
			{
				News::addOrderList($idHotel, $userId);
			}
			
		}

		return true;
	}


}