<?php
	return array(
		'news/([0-9]+)' => 'news/view/$1',
		'news' => 'news/index',
		// 'hotel/([0-9]+)' => 'hotel/view/$1',
		// 'hotel' => 'hotel/index',
		'reg' => 'reg/reg',
		'login' => 'login/login',
		'logout' => 'login/logout',
		// 'login/([0-9]+)' => 'login/user/$1',
		'user' => 'user/index',
		// 'user/([0-9]+)' => 'user/id/$1',
	);