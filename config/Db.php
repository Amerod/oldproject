<?php

class Db
{
	
	public static function getConnect()
	{
		$host = 'localhost';
		$dbname = 'test';
		$user = 'root';
		$pass = '';

		$db = new PDO("mysql:host=$host;port=3307;dbname=$dbname;", $user, $pass);

		return $db;
	}
}