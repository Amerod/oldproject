<?php

define('ROOT', dirname(__FILE__));

require_once (ROOT . '/control/Router.php');
require_once (ROOT . '/config/Db.php');

session_start();

$router = new Router();
$router->run();