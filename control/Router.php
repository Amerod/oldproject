<?php

class Router
{
	// хранит путь строки URL
	private $router;

	public function __construct()
	{
		$routerPath = ROOT . '\config\routes.php';
		$this->router = include($routerPath);
	}

	public function getURI()
	{
		if (!empty($_SERVER['REQUEST_URI'])) {
			return trim($_SERVER['REQUEST_URI'], '/');
		}
	}

	public function run()
	{
		// Получить структуру запроса
		$uri = $this->getURI();

		/**
		* preg_match — Выполняет проверку на соответствие выражению
		* uri — страка запроса
		* uriPattern — routes.php берутся ссылки (news - products)
		*/

		foreach ($this -> router as $uriPattern => $path) {
			if (preg_match("~$uriPattern~", $uri)) {

				// подставляем внутрений маршурт
				$internalRoute = preg_replace("~$uriPattern~", $path, $uri);

				$segments = explode('/', $internalRoute);

			/** Обработка строки URL
			* array_shift — использует первый элемент в массиве после удаляет его
			*/

				$controllerName = ucfirst(array_shift($segments)) . 'Controller';
				$actionsName = 'actions' . ucfirst(array_shift($segments));

				$parameters = $segments;



				/*
				* Проверка на существование файла
				* Подключение существующего файла
				*/
				
				// Проверка на существование файла
				$controllerFile = ROOT . '/controllers/' . $controllerName . '.php';

				if (file_exists($controllerFile)) {
					include_once($controllerFile);
				}
				// Подключение существующего файла
				$controllerObject = new $controllerName;
				$result = call_user_func_array(array($controllerObject, $actionsName), $parameters);

				if ($result != null) {
					break;
				}
			}
		}
	}
}