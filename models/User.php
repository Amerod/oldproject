<?php

class User
{
	//	Вывод личной информации пользователя
	public static function checkUserData($userId) {

		$db = Db::getConnect();

		$result = $db -> prepare("SELECT FName, LName, MName, DATE_FORMAT(birthday, GET_FORMAT(DATE,'EUR')) AS birthDay, phone FROM Users WHERE id = :userId");
		$result -> bindParam(':userId', $userId);
		$result-> execute();

		$UserInfo = $result->fetch(PDO::FETCH_OBJ);

		return $UserInfo;
	}

	public static function checkPassword($newPass) {
		if (strlen($newPass) >= 6) {
			return true;
		}
		return false;
	}

	public static function newPasswordUser($newPass) {
		if (strlen($newPass) >= 6) {
			return true;
		}
		return false;
	}

	public static function checkLName($LName) {
		if (strlen($LName) >= 2) {
			return true;
		}
		return false;
	}

	public static function checkFName($FName) {
		if (strlen($FName) >= 2) {
			return true;
		}
		return false;
	}

	public static function checkMName($MName) {
		if (strlen($MName) >= 2) {
			return true;
		}
		return false;
	}

	public static function checkBDay($BDay) {
		if (strlen($BDay) >= 10) {
			return true;
		}
		return false;
	}

	public static function checkPhone($phone) {
		if (strlen($phone) >= 8) {
			return true;
		}
		return false;
	}

	//	Смена пароля
	public static function updatePassword($userId, $oldPass, $newPass) {

		$db = Db::getConnect();

		$result = $db -> prepare('UPDATE Users SET password = :newPass WHERE id = :id AND password = :oldPass');
		$result -> bindParam(':newPass', $newPass);
		$result -> bindParam(':id', $userId);
		$result -> bindParam(':oldPass', $oldPass);
		$result-> execute();

		return $result;
		// переотправить пользователя на страницу авторизации (/login)
		// оборвать сессию
	}

	public static function updateProf($userId, $LName, $FName, $MName, $BDay, $phone) {
		
		$db = Db::getConnect();

		$result = $db -> prepare('UPDATE Users SET LName = :LName, FName = :FName, MName = :MName, birthday = :BDay, phone = :phone WHERE id = :id');

		$result -> bindParam(':id', $userId);
		$result -> bindParam(':LName', $LName);
		$result -> bindParam(':FName', $FName);
		$result -> bindParam(':MName', $MName);
		$result -> bindParam(':BDay', $BDay);
		$result -> bindParam(':phone', $phone);
		$result-> execute();

		return $result;
	}

	public static function OrdId($userId)
	{
		$db = Db::getConnect();

		$result = $db -> prepare("SELECT 'L.title' AS title, 'A.createDate' AS Dat, 'L.price' AS price, 'Status.title' AS status FROM Status, ListApartments L JOIN Applications A ON 'L.id' = 'A.hotelid' WHERE 'Status.id' = 'A.Status' AND 'A.userid' = :id");

		$result -> bindParam(':id', $userId);

		$result -> execute();

		$OrdUser = $result -> fetch(PDO::FETCH_OBJ);

		return $OrdUser;
	}

}