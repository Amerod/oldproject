<?php

class News
{
	/**
	*Возвращает один отель по ID
	*/

	public static function getNewsItemById($idHotel)
	{
		$db = Db::getConnect();

		$result = $db->query('SELECT id, title, fulltitle, UrlImg, price, kitchens, bed, bath FROM ListApartments WHERE id=' . $idHotel);

		$newsItem = $result->fetch(PDO::FETCH_OBJ);
		
		return $newsItem;
	}

	/**
	*Возвращает список отелей
	*/

	public static function getNewsList()
	{
		$db = Db::getConnect();

		$newsList = array();

		$result = $db->query('SELECT * FROM ListApartments');

		$i = 0;
		while ($row = $result->fetch(PDO::FETCH_OBJ)) {
			$newsList[$i]['id'] = $row->id;
			$newsList[$i]['UrlImg'] = $row->UrlImg;
			$newsList[$i]['price'] = $row->price;
			$newsList[$i]['title'] = $row->title;
			$newsList[$i]['kitchens'] = $row->kitchens;
			$newsList[$i]['bed'] = $row->bed;
			$newsList[$i]['bath'] = $row->bath;
			$i++;
		}
		return $newsList;
	}

	public static function addOrderList($idHotel, $userId)
	{
		// Проверка пользователя что он не гость
		// если гость отправить на страницу авторизации

		// добовление заявки
		$db = Db::getConnect();

		$result = $db -> prepare('INSERT INTO Applications (userid, createDate, hotelid, price) VALUES (:userId, NOW(), :idHotel, (SELECT price FROM ListApartments WHERE id = :idHotel))');

		$result -> bindParam(':userId', $userId);
		$result -> bindParam(':idHotel', $idHotel);

		$result -> execute();
	}
}