<?php

class Rule
{
	
	public static function Register($FName, $LName, $MName, $BDay, $phone, $password)
	{
		$db = Db::getConnect();

		$sql = ("INSERT INTO Users (FName, LName, MName, birthday, createDate, phone, password) VALUES (:FName, :LName, :MName, TIMESTAMP(str_to_date(:birthday, '%d.%m.%Y')), NOW(), :phone, :password)");
		$result = $db->prepare($sql);

		$result -> bindParam('FName', $FName);
		$result -> bindParam('LName', $LName);
		$result -> bindParam('MName', $MName);
		$result -> bindParam('birthday', $BDay);
		$result -> bindParam('phone', $phone);
		$result -> bindParam('password', $password);

		return $result -> execute();
	}
		// Проверка поля FName на наличие больше 2-х символов
		public static function checkFName($FName) {
			if (strlen($FName) >= 2) {
				return true;
				}
			return false;
		}

		public static function checkLName($LName) {
			if (strlen($LName) >= 2) {
				return true;
			}
			return false;
		}

		public static function checkMName($MName) {
			if (strlen($MName) >= 2) 
			{
				return true;
			}
			return false;
		}

		public static function checkBDay($BDay) { //решить проблему с маской даты
			if (strlen($BDay) >= 10) 
			{
				return true;
			}
			return false;
		}

		public static function checkPhone($phone) {
			if (strlen($phone) >= 8) 
			{
				return true;
			}
			return false;
		}

		public static function checkPhoneExists($phone)
		{
			$db = Db::getConnect();

			$sql = 'SELECT phone FROM Users WHERE phone = :phone';

			$stmt = $db->prepare($sql);
			$stmt -> bindParam(':phone', $phone);
			$stmt -> execute();

				if ($stmt->fetchColumn())
				{
					return true;
				}
			return false;
		}

		public static function checkPassword($password){
			if (strlen($password) >= 6) 
			{
				return true;
			}
			return false;
		}

}