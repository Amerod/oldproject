<?php

class Login
{
	
	public static function checkUserData($login, $password)
	{
		$db = Db::getConnect();

		$sql = 'SELECT id, phone, password FROM Users WHERE phone = :phone AND password = :password';

		$result = $db -> prepare($sql);
		$result -> bindParam(':phone', $login);
		$result -> bindParam(':password', $password);

		$result -> execute();

		$user = $result->fetch();
		if ($user) {
			return $user['id'];
		}

		return false;
	}

	public static function auth($userId) {
		$_SESSION['user'] = $userId;
	}

	// Если есть сессия вернуть Id пользователя
	public static function checkLogged() {
		if (isset($_SESSION['user'])) {
			return $_SESSION['user'];
		}

		header("Location: /login");
	}

	

	public static function isGuest() {
		if (isset($_SESSION['user'])) {
			return false;
		}

		return true;
	}

	public static function checkLogin($login) {
		if (strlen($login) >= 8) {
			return true;
		}
		return false;
	}

	public static function checkPassword($password) {
		if (strlen($password) >= 6) {
			return true;
		}
		return false;
	}

}